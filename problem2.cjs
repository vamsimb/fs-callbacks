const fs = require('fs');
const path = require('path')
const filenamesPath = path.join(__dirname, './filenames.txt')


function problem2() {
  fs.readFile(path.join(__dirname, 'lipsum.txt'), "utf-8", (err, data) => {
    console.log("Reading lipsum.txt file")

    if (err) {
      console.error(err);
    } else {
      console.log("Reading lipsum.txt successfull")
      let covertToUppercase = data.toString().toUpperCase()  // converting the data into uppercase
      fs.writeFile(path.join(__dirname, 'upperCase.txt'), covertToUppercase, (err, data) => {

        if (err) {
          console.error(err)
        } else {
          console.log('coverted into uppercase and stored in a uppercase.txt file')
          fs.writeFile(filenamesPath, 'upperCase.txt', (err) => {

            if (err) {
              console.error(err)
            } else {
              console.log('uppercase.txt file name stored in filenames.txt')
              fs.readFile(path.join(__dirname, 'upperCase.txt'), "utf-8", (err, data) => {
                console.log("Reading uppercase.txt file");

                if (err) {
                  console.error(err)
                } else {
                  console.log("Reading uppercase.txt successful");

                  let covertToLowercase = data.toString().toLowerCase() // converting the data intp lowercase
                  let splitData = covertToLowercase.split(". ").join("\n") // converted data splited into sentences
                  console.log("converted the data into sentences")
                  fs.writeFile(path.join(__dirname, 'splitedIntoSentences.txt'), splitData, (err, data) => {

                    if (err) {
                      console.error(err)
                    } else {
                      console.log("created splitedIntoSentences.txt file and stored the converted data")
                      fs.appendFile(filenamesPath, ' splitedIntoSentences.txt', (err, data) => {

                        if (err) {
                          console.error(err)
                        } else {
                          console.log('splitedIntoSentences.txt file name stored in filenames.txt')
                          fs.readFile(path.join(__dirname, 'splitedIntoSentences.txt'), "utf-8", (err, data) => {
                            console.log("Reading splitedIntoSentences.txt file");

                            if (err) {
                              console.error(err)
                            } else {
                              console.log("Reading splitedIntoSentences.txt file successfull")
                              let sortData = data.toString().split("\n").filter(sentences => {

                                if (sentences) {
                                  return sentences.trim() // trims if any unwanted spaces exists at the ends
                                }
                              }).sort().join('\n') // // sort and joins the data
                              console.log(sortData)
                              console.log("sorted the data from splitedIntoSentences.txt")
                              fs.writeFile(path.join(__dirname, 'sortedData.txt'), sortData, (err) => {

                                if (err) {
                                  console.error(err)
                                } else {
                                  console.log("created sortedData.txt file and stored the sorted data")

                                  fs.appendFile(filenamesPath, ' sortedData.txt', (err, data) => {

                                    if (err) {
                                      console.error(err)
                                    } else {
                                      console.log('sortedData.txt file name stored in filenames.txt')
                                      console.log("Reading filenames.txt")
                                      fs.readFile(filenamesPath, "utf-8", (err, data) => {

                                        if (err) {
                                          console.error()
                                        } else {
                                          console.log("Reading filenames.txt file successful")
                                          console.log("deleting files which are written in filenames.txt")
                                          data.toString().split(" ").map(file => {
                                            fs.unlink(path.join(__dirname, file), (err) => {

                                              if (err) {
                                                console.error(err)
                                              } else {
                                                console.log(`${file} successfully deleted`)
                                              }
                                            })
                                          })
                                        }
                                      })
                                    }
                                  })
                                }
                              })
                            }
                          })
                        }
                      })
                    }
                  })
                }
              });
            }
          });
        }
      })
    }
  })
}


module.exports = problem2;