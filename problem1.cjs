const fs = require('fs');

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


function creatingRandomJsonFiles() {
  let fileCount = Math.floor(Math.random() * 10) + 1
  //creating directory
  fs.mkdir('randomFilesDirectory', { recursive: true }, (err) => {
    console.log('Creating Random Files Directory')

    if (err) {
      console.log(err)
    } else {
      console.log('Random Files Directory created')
      let length = Array(fileCount).fill(0) // which fills an array like this [0,0,0,....]
      let count = 0
      length.map((data, index) => {         // used map to get index numbers
        let filedata = `I am json file-${index + 1}` // created some data to store in the file
        fs.writeFile(`randomFilesDirectory/file-${index + 1}.json`, filedata, (err) => {

          if (err) {
            console.error(err)
          } else {
            console.log(`file-${index + 1}.json has been created`);
            count += 1                    // counting when ever file creates

            if (count == fileCount) {
              console.log("All random files are created in a directory")
              let count = 0
              length.map((data, index) => {
                fs.unlink(`randomFilesDirectory/file-${fileCount - index}.json`, (err) => {

                  if (err) {
                    console.error(err)
                  } else {
                    console.log(`file-${fileCount - index}.json has been deleted`)
                    count += 1

                    if (count == fileCount) {
                      fs.rmdir("randomFilesDirectory", (err) => {

                        if (err) {
                          console.log(err)
                        } else {
                          console.log("Random Files Directory is deleted")
                        }
                      })
                    }
                  }
                })
              })
            }
          }
        });
      })
    }
  });
}



module.exports = creatingRandomJsonFiles;